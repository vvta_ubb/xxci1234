package BibliotecaApp.repository;

import static org.junit.Assert.*;

import BibliotecaApp.repo.CartiRepo;
import BibliotecaApp.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import BibliotecaApp.control.BibliotecaCtrl;
import BibliotecaApp.model.Carte;
import java.util.ArrayList;

public class CartiRepoTest {

	private BibliotecaCtrl controller;
	private CartiRepoMock repository;
	private int previousRepoSize = 0;
	
	@Before
	public void setUp() throws Exception {
		repository = new CartiRepoMock();
		controller = new BibliotecaCtrl(repository);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetSize_Valid() {
		try {
			assertEquals(previousRepoSize, controller.getCarti().size());
		} catch (Exception e) {
			e.printStackTrace();
			assertEquals(0, 1);
		}
	}
	
	@Test
	public void testAdaugaCarte_Valid() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor1");
		autori.add("Autor2");
		c.setAutori(autori);
		
		c.setAnAparitie("2019");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword1");
		keywords.add("keyword2");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(previousRepoSize+1, controller.getCarti().size());
		} catch (Exception e) {
			e.printStackTrace();
			assertEquals(0, 1);
		};
	}
	
	@Test
	public void testAdaugaCarte_TitleInvalid() {
		Carte c = new Carte();
		
		c.setTitlu("");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor1");
		autori.add("Autor2");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword1");
		keywords.add("keyword2");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Titlu invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte_FaraAutori() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword1");
		keywords.add("keyword2");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Lista autori vida!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte_AutorInvalid() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword1");
		keywords.add("keyword2");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Autor invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte_AnAparitieInvalid() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor1");
		autori.add("Autor2");
		c.setAutori(autori);
		
		c.setAnAparitie("aaaa");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword1");
		keywords.add("keyword2");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("An aparitie invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte_EdituraInvalid() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor1");
		autori.add("Autor2");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("keyword1");
		keywords.add("keyword2");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Editura invalid!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte_FaraCuvinteCheie() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor1");
		autori.add("Autor2");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, controller.getCarti().size());
		} catch (Exception e) {
			//e.printStackTrace();
			//assertEquals(0, 1);
			assertEquals("Lista cuvinte cheie vida!", e.getMessage());
		};
	}
	
	@Test
	public void testAdaugaCarte_CuvantCheieInvalid() {
		Carte c = new Carte();
		
		c.setTitlu("Book title");
		
		ArrayList<String> autori = new ArrayList<>();
		autori.add("Autor1");
		autori.add("Autor2");
		c.setAutori(autori);
		
		c.setAnAparitie("2017");
		
		c.setEditura("Editura");
		
		ArrayList<String> keywords = new ArrayList<>();
		keywords.add("");
		c.setCuvinteCheie(keywords);
		
		try {
			controller.adaugaCarte(c);
			assertEquals(0, 1);
		} catch (Exception e) {
			assertEquals("Cuvant cheie invalid!", e.getMessage());
		};
	}
	


}